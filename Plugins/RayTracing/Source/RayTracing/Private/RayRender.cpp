#include "RayRender.h"
#include "GlobalShader.h"
#include "Shader.h"
#include "ShaderCompilerCore.h"

BEGIN_GLOBAL_SHADER_PARAMETER_STRUCT(FRTXOutputSurface, )
SHADER_PARAMETER(FVector, OutputSurface)
END_GLOBAL_SHADER_PARAMETER_STRUCT()

class FRayComputeShader : public FGlobalShader
{
    DECLARE_SHADER_TYPE(FRayComputeShader, Global);

public:
    LAYOUT_FIELD(FShaderResourceParameter, OutputSurface);
    
    FRayComputeShader(){}

    FRayComputeShader(const ShaderMetaType::CompiledShaderInitializerType &Initializer) : FGlobalShader(Initializer)
    {
        OutputSurface.Bind(Initializer.ParameterMap, TEXT("OutputSurface"));
    }

    static bool ShouldCache(EShaderPlatform Platform)
    {
        return IsFeatureLevelSupported(Platform, ERHIFeatureLevel::SM5);
    }

    static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
    {
        return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
    }
    
    static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Platform, FShaderCompilerEnvironment& OutEnvironment)
    {
        FGlobalShader::ModifyCompilationEnvironment(Platform, OutEnvironment);
        OutEnvironment.CompilerFlags.Add(CFLAG_StandardOptimization);
    }

    void SetSurfaces(FRHICommandList& RHICmdList, FUnorderedAccessViewRHIRef OutputSurfaceUAV)
    {
        TShaderMapRef<FRayComputeShader> cs(GetGlobalShaderMap(ERHIFeatureLevel::SM5));
        FComputeShaderRHIRef ComputeShaderRHI = cs.GetComputeShader();

        if (OutputSurface.IsBound())
        {
            RHICmdList.SetUAVParameter(ComputeShaderRHI, OutputSurface.GetBaseIndex(), OutputSurfaceUAV);
        }
    }

    void UnbindBuffers(FRHICommandList& RHICmdList)
    {
        TShaderMapRef<FRayComputeShader> cs(GetGlobalShaderMap(ERHIFeatureLevel::SM5));
        FComputeShaderRHIRef ComputeShaderRHI = cs.GetComputeShader();

        if (OutputSurface.IsBound())
        {
            RHICmdList.SetUAVParameter(ComputeShaderRHI, OutputSurface.GetBaseIndex(), FUnorderedAccessViewRHIRef());
        }
    }

};

IMPLEMENT_SHADER_TYPE(, FRayComputeShader, TEXT("/Plugin/Raytracing/Private/Raytracing.usf"), TEXT("MainCS"), SF_Compute);

static void RayTracing_RenderThread(
                                    FRHICommandListImmediate& RHICmdList,
                                    ERHIFeatureLevel::Type FeatureLevel
)
{
    check(IsInRenderingThread());
    TArray<FVector4> Bitmap;

    TShaderMapRef<FRayComputeShader> ComputeShader(GetGlobalShaderMap(FeatureLevel));
    RHICmdList.SetComputeShader(ComputeShader.GetComputeShader());

    int32 SizeX = 256;
    int32 SizeY = 256;

    FRHIResourceCreateInfo CreateInfo;
    FTexture2DRHIRef Texture = RHICreateTexture2D(SizeX, SizeY, PF_A32B32G32R32F, 1, 1, TexCreate_ShaderResource | TexCreate_UAV, CreateInfo);

    FUnorderedAccessViewRHIRef TextureUAV = RHICreateUnorderedAccessView(Texture);
    ComputeShader->SetSurfaces(RHICmdList, TextureUAV);
    DispatchComputeShader(RHICmdList, ComputeShader, SizeX / 32, SizeY / 32, 1);
}

void ARayRender::MainRayRender()
{
}

