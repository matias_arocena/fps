// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "GeneratedScene.generated.h"

UENUM(BlueprintType)
enum class ECell : uint8
{
    Empty UMETA(DisplayName="Empty"),
    Walkable UMETA(DisplayName="Walkable"),
    Wall UMETA(DisplayName="Wall")
};

/**
 * 
 */
UCLASS()
class AGeneratedScene : public AStaticMeshActor
{
	GENERATED_BODY()

public:
    AGeneratedScene(const FObjectInitializer& ObjectInitializer);

    virtual void BeginPlay() override;
private:
    UPROPERTY(EditAnywhere)
    UStaticMesh* FloorStaticMesh;

    UPROPERTY(EditAnywhere)
    int MazeSize;

    USceneComponent* Scene;

    const int8 UP = 0;
    const int8 RIGHT = 1;
    const int8 DOWN = 2;
    const int8 LEFT = 3;
    
    ECell** GenerateMaze();

    void GenerateMazeDFS(ECell** Maze, int X, int Y);
    
    void ParseMazeMatrix(TUniquePtr<ECell*> Maze);
};
