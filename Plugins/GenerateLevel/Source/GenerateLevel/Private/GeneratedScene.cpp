// Fill out your copyright notice in the Description page of Project Settings.


#include "GeneratedScene.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "UObject/ConstructorHelpers.h"


AGeneratedScene::AGeneratedScene(const FObjectInitializer &ObjectInitializer)
    : Super(ObjectInitializer) {
  Scene = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(
      this, TEXT("Root"));
  if (RootComponent) {
    Scene->AttachToComponent(RootComponent,
                             FAttachmentTransformRules::KeepRelativeTransform);
  } else {
    RootComponent = Scene;
  }
}

void AGeneratedScene::BeginPlay()
{
    TUniquePtr<ECell*>Maze(GenerateMaze());

    ParseMazeMatrix(std::move(Maze));
}

ECell** AGeneratedScene::GenerateMaze()
{
    ECell** Maze = new ECell*[MazeSize];
    for (int i = 0; i<MazeSize; i++)
    {
        Maze[i] = new ECell[MazeSize];
        for (int j = 0; j < MazeSize; j++)
        {
            Maze[i][j] = ECell::Empty;
        }
    }

    GenerateMazeDFS(Maze, 0, 0);

    for (int i = 0; i < MazeSize; i++)
    {
        FString MazeLine = "";
        for (int j = 0; j < MazeSize; j++)
        {
            if(Maze[i][j] == ECell::Walkable)
            {
                MazeLine += "1";
            } else {
                MazeLine += "0";
            }
        }
        UE_LOG(LogTemp, Warning, TEXT("%s"), *MazeLine);
    }
    return Maze;
}

void AGeneratedScene::GenerateMazeDFS(ECell** Maze, int X, int Y)
{
    Maze[X][Y] = ECell::Walkable;
    TArray<int8> TriedDirections;

    while (TriedDirections.Num() < 4) {
        int8 CurrentDirection = FMath::RandRange(0, 3);
        UE_LOG(LogTemp, Warning, TEXT("Direction %i in (%i, %i)"), CurrentDirection, X, Y);
        if (!TriedDirections.Contains(CurrentDirection))
        {
            TriedDirections.Add(CurrentDirection);
            if (CurrentDirection == UP && Y != 0 && Maze[X][Y-1] == ECell::Empty)
            {
                if (X > 0 && Maze[X-1][Y] == ECell::Empty)  Maze[X-1][Y] = ECell::Wall;
                if (X < MazeSize - 1 && Maze[X+1][Y] == ECell::Empty) Maze[X+1][Y] = ECell::Wall;
                if (Y < MazeSize -1 && Maze[X][Y+1] == ECell::Empty) Maze[X][Y+1] = ECell::Wall;
                GenerateMazeDFS(Maze, X, Y-1);
            }
            else if (CurrentDirection == DOWN && Y != MazeSize-1 && Maze[X][Y+1] == ECell::Empty)
            {
                if (X > 0 && Maze[X-1][Y] == ECell::Empty)  Maze[X-1][Y] = ECell::Wall;
                if (X < MazeSize - 1 && Maze[X+1][Y] == ECell::Empty) Maze[X+1][Y] = ECell::Wall;
                if (Y > 0 && Maze[X][Y-1] == ECell::Empty) Maze[X][Y-1] = ECell::Wall;
                GenerateMazeDFS(Maze, X, Y+1);
            }
            else if (CurrentDirection == RIGHT && X != MazeSize-1 && Maze[X+1][Y] == ECell::Empty)
            {
                if (Y > 0 && Maze[X][Y-1] == ECell::Empty)  Maze[X][Y-1] = ECell::Wall;
                if (Y < MazeSize - 1 && Maze[X][Y+1] == ECell::Empty) Maze[X][Y+1] = ECell::Wall;
                if (X > 0 && Maze[X-1][Y] == ECell::Empty) Maze[X-1][Y] = ECell::Wall;
                GenerateMazeDFS(Maze, X+1, Y);
            }
            else if (CurrentDirection == LEFT && X != 0 && Maze[X-1][Y] == ECell::Empty)
            {
                if (Y > 0 && Maze[X][Y-1] == ECell::Empty)  Maze[X][Y-1] = ECell::Wall;
                if (Y < MazeSize - 1 && Maze[X][Y+1] == ECell::Empty) Maze[X][Y+1] = ECell::Wall;
                if (X < MazeSize -1 && Maze[X+1][Y] == ECell::Empty) Maze[X+1][Y] = ECell::Wall;
                GenerateMazeDFS(Maze, X-1, Y);
            }
        }
    }
}

void AGeneratedScene::ParseMazeMatrix(TUniquePtr<ECell*> Maze)
{
    if(!FloorStaticMesh)
    {
        return;
    }

    for (int i = 0; i < MazeSize; i++)
    {
        for (int j = 0; j < MazeSize; j++)
        {
            if(Maze.Get()[i][j] == ECell::Walkable)
            {
                UStaticMeshComponent* Mesh = NewObject<UStaticMeshComponent>(this);
                FVector MeshSize = FloorStaticMesh->GetBounds().GetBox().GetSize();
                Mesh->SetRelativeLocation(FVector(i * MeshSize.X, j * MeshSize.Y, 0));
                Mesh->AttachToComponent(Scene, FAttachmentTransformRules::KeepRelativeTransform);
                Mesh->RegisterComponent();
                Mesh->SetStaticMesh(FloorStaticMesh);
            }
        }
    }
}
