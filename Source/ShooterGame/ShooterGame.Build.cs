// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ShooterGame : ModuleRules
{
	public ShooterGame(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            "AIModule",
            "GameplayTasks",
            "OnlineSubsystem",
            "OnlineSubsystemUtils"
		});


        PrivateDependencyModuleNames.AddRange(new string[] {  });

		PublicIncludePaths.AddRange(new string[] {
			"ShooterGame/Public/Actors",
			"ShooterGame/Public/AI",
			"ShooterGame/Public/Characters",
			"ShooterGame/Public/Controllers",
			"ShooterGame/Public/GameModes",
            "ShooterGame/Public/Models",
			"ShooterGame/Public/UI/HUDs",
			"ShooterGame/Public/UI/Widgets",
            "ShooterGame/Public/ViewModels"
		});
		
		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
