// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "FriendsModel.h"

#include "FriendsViewModel.generated.h"

USTRUCT()
struct FFriendTooltipView {
    GENERATED_BODY()

public:
    FString Name;

    EPlatform Platform;

    FDateTime LastTimeOnline;

    bool bConnected;
};

UCLASS()
class SHOOTERGAME_API UFriendsListItemView : public UObject {
    GENERATED_BODY()
public:
    FString Name;

    EPlatform Platform;
};

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UFriendsViewModel : public UObject
{
	GENERATED_BODY()
public:
    void SetDB(UFriendsModel* FriendsModel);

    TArray<UFriendsListItemView*> GetOnlineFriends();
    
    TArray<UFriendsListItemView*> GetOfflineFriends();

    FFriendTooltipView GetFriend(const FName& Id);

    DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnPlayerConnectionEvent, FName, FriendName, bool, bIsConnecting);
    FOnPlayerConnectionEvent OnPlayerConnectionEvent;
private:
    UFriendsModel* DB;

    void TryDBConnection();

    TArray<UFriendsListItemView*> GetFriendsStrategy(bool bShowOnline);

    UFUNCTION()
    void OnFriendConnectionEvent(FName FriendName, bool bIsConnecting);
};
