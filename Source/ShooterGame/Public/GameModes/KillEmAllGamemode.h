// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterGameGameModeBase.h"
#include "KillEmAllGamemode.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API AKillEmAllGamemode : public AShooterGameGameModeBase
{
	GENERATED_BODY()

public:
	virtual void PawnKilled(APawn* PawnKilled) override;

private:
	void EndGame(const bool bIsPlayerWinner) const;
};
