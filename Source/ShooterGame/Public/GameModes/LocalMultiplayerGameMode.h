// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameModes/ShooterGameGameModeBase.h"
#include "LocalMultiplayerGameMode.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API ALocalMultiplayerGameMode : public AShooterGameGameModeBase
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;
	virtual void PawnKilled(APawn* PawnKilled) override;

private:
	APlayerController* SecondPlayer;

	void EndGame(const APlayerController* WinnerId) const;
};
