// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterGameGameModeBase.h"

#include "HAL/Platform.h"
#include "GameFramework/GameMode.h"
#include "NetworkGameMode.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API ANetworkGameMode : public AShooterGameGameModeBase
{
	GENERATED_BODY()

public:
	ANetworkGameMode();
	
	virtual void PawnKilled(APawn* PawnKilled) override;

protected:
	virtual void PostLogin(APlayerController* NewPlayer) override;

	virtual void Logout(AController* Exiting) override;

	virtual bool ReadyToStartMatch_Implementation() override;

	virtual void StartMatch() override;
private:
	FCriticalSection* CountPlayerMutex;

	uint8 RemainingPlayers;

	uint8 CountPlayers() const;
};
