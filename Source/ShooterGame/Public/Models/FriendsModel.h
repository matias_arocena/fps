// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <atomic>

#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "FriendsModel.generated.h"

UENUM(BlueprintType)
enum class EPlatform : uint8
{
	WIN UMETA(DisplayName="Windows"),
    UNIX UMETA(DisplayName="Unix"),
	PS UMETA(DisplayName="PlayStation"),
    SWITCH UMETA(DisplayName="Switch")
};


USTRUCT()
struct FFriends : public FTableRowBase {
    GENERATED_BODY()

public:
    FString Name;

    EPlatform Platform;

    bool bConnected;

    FDateTime LastConnection;
};

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UFriendsModel : public UObject
{
	GENERATED_BODY()
public:
    bool Connect();

    bool IsConnected() const;
    
    TArray<FFriends *> GetFriendsList() const;

    FFriends* GetFriend(FName Id) const;

    void Shutdown();
    
    DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPlayerConnectionEvent, FName, Row, bool, bIsConnecting);
    FPlayerConnectionEvent OnFriendConnectionEvent;
private:
    bool bConnected;
    
    std::atomic_bool bIsGameRunning;

	FTimerHandle UpdateDBTimer;

    mutable FRWLock FriendsDataMutex;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
    UDataTable* FriendsData;

    FFriends GenerateUser(int Id) const;
    
    void LoadDB();

    void UpdateDB();

    void ToggleConnection(const FName& Row) const;
};

