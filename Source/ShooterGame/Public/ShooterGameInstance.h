// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Interfaces/OnlineSessionInterface.h"

#include "FriendsModel.h"
#include "FriendsViewModel.h"

#include "ShooterGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UShooterGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UShooterGameInstance();

	virtual void Init() override;

	UFUNCTION()
	void JoinServer();

	UFUNCTION()
	void DestroySession();

    UFriendsViewModel* GetFriendsViewModel();

    virtual void Shutdown() override;
private:
	UPROPERTY(EditAnywhere)
	FSoftObjectPath MultiPlayerLevel;

	UPROPERTY(EditAnywhere)
	FName SessionName;

	IOnlineSessionPtr OnlineSession;

	TSharedPtr<FOnlineSessionSearch> SessionSearch;

    FCriticalSection* FriendsModelInstanceMutex;
    
    UPROPERTY()
    UFriendsModel* FriendsModelInstance;

    FCriticalSection* FriendsViewModelInstanceMutex;

    UPROPERTY()
    UFriendsViewModel* FriendsViewModelInstance;
    
    UFriendsModel* GetFriendsModel();
    
	void CreateServer() const;

	void ConnectToSession();

	void SelfDestruct() const;

	virtual void OnCreateSessionComplete(FName ServerName, bool bSucceeded);
	virtual void OnFindSessionsComplete(bool bSucceeded);
	virtual void OnJoinSessionComplete(FName ServerName, EOnJoinSessionCompleteResult::Type Result);

};
