// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "InGameHUD.h"
#include "WaitScreenWidget.h"

#include "NetworkInGameHUD.generated.h"

class UWaitScreenWidget;
/**
 * 
 */
UCLASS()
class SHOOTERGAME_API ANetworkInGameHUD : public AInGameHUD
{
	GENERATED_BODY()
public:
	ANetworkInGameHUD();

	virtual void StartGame() override;

	virtual void EndGame(const bool bIsWinner, const int Count) override;

	void WaitUntilMatchEnds();

	UFUNCTION()
	void StartMatch();

	UPROPERTY(EditDefaultsOnly, Category="Widgets")
	TSubclassOf<UUserWidget> WaitingWidgetClass;
private:
	UPROPERTY()
	UWaitScreenWidget* WaitScreenWidget;

};
