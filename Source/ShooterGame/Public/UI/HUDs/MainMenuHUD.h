// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"

#include "MainMenuWidget.h"

#include "MainMenuHUD.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API AMainMenuHUD final : public AHUD
{
	GENERATED_BODY()
	
public:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category="Widgets")
	TSubclassOf<UUserWidget> MainMenuWidgetClass;
private:
	UPROPERTY()
	UMainMenuWidget* MainMenuWidget;
};
