// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ConnectedFriendsWidget.h"
#include "CoreMinimal.h"
#include "FriendEventWidget.h"
#include "GameFramework/HUD.h"
#include "Components/WidgetComponent.h"

#include "InGameHUD.generated.h"

class UEndGameWidget;
class UInGameWidget;

UENUM(BlueprintType)
enum class EGameState : uint8
{
	Waiting UMETA(DisplayName="Waiting"),
	Playing UMETA(DisplayName="Playing"),
	EndGame UMETA(DisplayName="EndGame")
};

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API AInGameHUD : public AHUD
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditDefaultsOnly, Category="Widgets")
	TSubclassOf<UUserWidget> InGameWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category="Widgets")
	TSubclassOf<UUserWidget> EndGameWidgetClass;

    UPROPERTY(EditDefaultsOnly, Category="Widgets")
	TSubclassOf<UUserWidget> FriendsWidgetClass;
    
    UPROPERTY(EditDefaultsOnly, Category="Widgets")
	TSubclassOf<UUserWidget> FriendsEventWidgetClass;

	AInGameHUD();

	explicit AInGameHUD(EGameState State);

	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void StartGame();

	UFUNCTION()
	virtual void EndGame(const bool bIsWinner, const int Count);

	UFUNCTION()
	virtual void SetHealthPercentage(float HealthPercentage) const;
	
    void ToggleFriendsWidget();
    
	void DecreaseCount() const;
protected:
	EGameState State;

	FCriticalSection* StateMutex;

	UPROPERTY()
	UInGameWidget* InGameWidget;

	UPROPERTY()
	UEndGameWidget* EndGameWidget;

    UPROPERTY()
    UConnectedFriendsWidget* FriendsWidget;

    UPROPERTY()
    UFriendEventWidget* FriendsEventWidget;
};
