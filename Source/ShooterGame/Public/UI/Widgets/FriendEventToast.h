// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/TextBlock.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "FriendEventToast.generated.h"

/**
* 
*/
UCLASS()
class SHOOTERGAME_API UFriendEventToast : public UUserWidget {
    GENERATED_BODY()

public:
    void SetText(const FString& Name);

    virtual void ReleaseSlateResources(bool bRealeaseChildrens) override;
private:
    UPROPERTY(EditAnywhere, meta = (BindWidget))
    UTextBlock* PlayerName;
};
