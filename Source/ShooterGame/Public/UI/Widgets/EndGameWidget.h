// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "Runtime/UMG/Public/UMG.h"

#include "EndGameWidget.generated.h"

UENUM(BlueprintType)
enum class EScreenType : uint8
{
	WinScreen UMETA(DisplayName="WinScreen"),
	LoseScreen UMETA(DisplayName="LoseScreen")
};

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UEndGameWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UEndGameWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TxtEndGameTitle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TxtEndGameText;

	void SetScreen(const EScreenType ScreenType, const int TotalSeconds);

	virtual void DecreaseCount();
	
protected:
	UPROPERTY(EditAnywhere, Category = "Design")
	FText WinTitle;

	UPROPERTY(EditAnywhere, Category = "Design")
	FText LoseTitle;

	UPROPERTY(EditAnywhere, Category = "Design")
	FText Text;

	UPROPERTY(EditAnywhere, Category = "Design")
	FLinearColor WinColor;

	UPROPERTY(EditAnywhere, Category = "Design")
	FLinearColor LoseColor;

	int Count;
};
