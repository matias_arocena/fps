// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/TextBlock.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "ViewModels/FriendsViewModel.h"

#include "FriendTooltip.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UFriendTooltip : public UUserWidget
{
	GENERATED_BODY()
	
public:
    void SetFriend(const FFriendTooltipView& Friend);

private:
    UPROPERTY(EditAnywhere, meta=(BindWidget))
    UTextBlock* FriendName;

    UPROPERTY(EditAnywhere, meta=(BindWidget))
    UTextBlock* ConnectionStatus;
};
