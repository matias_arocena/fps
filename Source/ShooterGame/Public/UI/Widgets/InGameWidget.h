// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/ProgressBar.h"
#include "Runtime/UMG/Public/UMG.h"


#include "InGameWidget.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UInGameWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UInGameWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	UProgressBar* HealthBar;

	void SetHealthPercentage(float HealthPercentage);

};
