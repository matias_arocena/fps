// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/IUserObjectListEntry.h"
#include "Blueprint/UserWidget.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "CoreMinimal.h"

#include "FriendListEntryWidget.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UFriendListEntryWidget : public UUserWidget, public IUserObjectListEntry
{
	GENERATED_BODY()

public:
    virtual void NativeOnListItemObjectSet(UObject* ListItemObject);

private:
    UPROPERTY(EditAnywhere, meta = (BindWidget))
    UTextBlock* FriendNameText;

    UPROPERTY(EditAnywhere, meta = (BindWidget))
    UImage* PlatformIcon;

    UPROPERTY(EditAnywhere)
    TSoftObjectPtr<UTexture2D> PCIcon;

    UPROPERTY(EditAnywhere)
    TSoftObjectPtr<UTexture2D> UnixIcon;

    UPROPERTY(EditAnywhere)
    TSoftObjectPtr<UTexture2D> PSIcon;

    UPROPERTY(EditAnywhere)
    TSoftObjectPtr<UTexture2D> SwitchIcon;

    void SetImageFromIcon(const TSoftObjectPtr<UTexture2D>& Icon);
};
