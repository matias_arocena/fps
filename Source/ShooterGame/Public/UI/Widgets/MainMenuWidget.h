// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Animation/WidgetAnimation.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"

#include "MainMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UMainMenuWidget final : public UUserWidget
{
	GENERATED_BODY()

public:
	UMainMenuWidget(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	UButton* StartSinglePlayerButton;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	UButton* JoinMultiPlayerButton;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	UButton* StartLocalMultiPlayerButton;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	UButton* QuitButton;

protected:
	virtual void NativeConstruct() override;

private:
	UPROPERTY(EditAnywhere)
	FSoftObjectPath SinglePlayerLevel;

	UPROPERTY(EditAnywhere)
	FSoftObjectPath LocalMultiPlayerLevel;
	
	UFUNCTION()
	void StartSinglePlayerGame();

	UFUNCTION()
	void JoinMultiPlayerGame();

	UFUNCTION()
	void StartLocalMultiPlayerGame();

	UFUNCTION()
	void QuitGame();
};
