// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/Button.h"
#include "Components/ListView.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "FriendTooltip.h"
#include "FriendsViewModel.h"

#include "ConnectedFriendsWidget.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UConnectedFriendsWidget : public UUserWidget
{
	GENERATED_BODY()
public:
    virtual void NativeConstruct() override;

    UFUNCTION(BlueprintImplementableEvent)
    void OnBeforeToggleList(UListView* List, bool bIsCollapsing);

    UFUNCTION(BlueprintImplementableEvent)
    void OnAfterToggleList(UListView* List, bool bIsCollapsing);
  
    virtual void ReleaseSlateResources(bool bReleaseChildren) override;
                                     
private:
    UPROPERTY(EditAnywhere, meta = (BindWidget))
    UListView* OnlineFriendsList;
    
    UPROPERTY(EditAnywhere, meta = (BindWidget))
    UListView* OfflineFriendsList;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UButton* ToggleOnlineListButton;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	UButton* ToggleOfflineListButton;

    UPROPERTY(EditAnywhere)
    TSubclassOf<UUserWidget> FriendTooltipClass;

    UPROPERTY()
    UFriendTooltip* FriendTooltip;

    UFriendsViewModel* ViewModel;

    UFUNCTION()
    void ToggleOnlineList();

    UFUNCTION()
    void ToggleOfflineList();

    UFUNCTION()
    void OnFriendConnectionEvent(FName FriendName, bool bIsConnecting);

    UFUNCTION()
    void OnHoverFriend(UObject* FriendEntry, bool bIsOnline);
};
