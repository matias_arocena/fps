// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "WaitScreenWidget.generated.h"

UENUM(BlueprintType)
enum class EWaitMode : uint8
{
	WaitingForPlayers UMETA(DisplayName="WaitingForPlayers"),
	WaitingForEnd UMETA(DisplayName="WaitingForEnd")
};
/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UWaitScreenWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UWaitScreenWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TxtWaitingTitle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TxtWaitingText;

	void SetScreen(const EWaitMode WaitMode);

private:
	UPROPERTY(EditAnywhere, Category="Design")
	FText WaitTitle;

	UPROPERTY(EditAnywhere, Category="Design")
	FText LoseTitle;
	
	UPROPERTY(EditAnywhere, Category="Design")
	FText WaitText;

	UPROPERTY(EditAnywhere, Category="Design")
	FText LoseText;

	UPROPERTY(EditAnywhere, Category="Design")
	FLinearColor WaitColor;

	UPROPERTY(EditAnywhere, Category="Design")
	FLinearColor LoseColor;
};
