// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Blueprint/UserWidget.h"

#include "Blueprint/UserWidgetPool.h"
#include "Components/VerticalBox.h"
#include "CoreMinimal.h"

#include "FriendEventToast.h"

#include "FriendEventWidget.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API UFriendEventWidget : public UUserWidget
{
	GENERATED_BODY()
public:
    UPROPERTY(EditDefaultsOnly, Category="Widgets")
    TSubclassOf<UUserWidget> FriendEventToastWidgetClass;

    UPROPERTY()
    FUserWidgetPool WidgetPool;

    UPROPERTY(EditAnywhere, meta = (BindWidget))
    UVerticalBox* VerticalBox;

    FCriticalSection* ToastsMutex;
    
    TArray<UFriendEventToast*> Toasts;

    UPROPERTY(EditAnywhere)
    float ToastDuration;

    virtual void NativeDestruct() override;

    UFUNCTION(BlueprintImplementableEvent)
    void OnAfterNewToast();

    UFUNCTION(BlueprintImplementableEvent)
    void OnBeforeNewToast();

    UFUNCTION(BlueprintImplementableEvent)
    void OnBeforeClearToast();
    
    UFUNCTION(BlueprintImplementableEvent)
    void OnAfterClearToast();

    UFUNCTION()
    void OnFriendConnectionEvent(FName Id, bool bJustConnected);
private:
    void ClearToast();    
};
