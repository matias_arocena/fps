// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionTypes.h"
#include "ShooterAIController.generated.h"

class UBehaviorTree;

UCLASS()
class SHOOTERGAME_API AShooterAIController final : public AAIController
{
	GENERATED_BODY()
public:
	bool IsDead() const;

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere)
	UBehaviorTree* AIBehavior;

    UFUNCTION()
    void OnStimulus(AActor* Actor, FAIStimulus Stimulus);

    UPROPERTY(EditAnywhere)
    FName LastSeenBlackboardProperty;
};
