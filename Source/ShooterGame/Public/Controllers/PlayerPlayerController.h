#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PlayerPlayerController.generated.h"

class UUserWidget;

/**
 * 
 */
UCLASS()
class SHOOTERGAME_API APlayerPlayerController final : public APlayerController
{
	GENERATED_BODY()
public:
	APlayerPlayerController();

	UFUNCTION(Client, Reliable)
	virtual void GameHasEnded(class AActor* EndGameFocus = nullptr, bool bIsWinner = false) override;

	UFUNCTION(Client, Reliable)
	void Kill();

	UFUNCTION(Client, Reliable)
	void MatchStarted();

	bool IsDead() const;

	virtual void SpawnDefaultHUD() override;
private:

	UPROPERTY(EditAnywhere)
	FSoftObjectPath MainMenu;

	UPROPERTY(EditAnywhere)
	int RestartDelay;

	UPROPERTY()
	UUserWidget* HUDScreen;

	FTimerHandle RestartTimer;

	void DecreaseRestartCount();
};
