// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Gun.h"
#include "Rock.h"

#include "ShooterCharacter.generated.h"


UCLASS()
class SHOOTERGAME_API AShooterCharacter final : public ACharacter
{
	GENERATED_BODY()

public:
	AShooterCharacter();

	UFUNCTION(BlueprintPure)
	bool IsDead() const;

	UFUNCTION(BlueprintPure)
	float GetHealthPercentage() const;

	void SetHealth(float HealthValue);

	UFUNCTION()
	void OnAbilityNotify();
	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void ShotGun();

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

protected:
	UPROPERTY(EditDefaultsOnly)
	float MaxHealth;

	UPROPERTY(VisibleAnywhere, ReplicatedUsing=OnHealthUpdate)
	float Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Animations")
	UAnimMontage* AbilityMontage;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnHealthUpdate();

	UFUNCTION(Server, Reliable)
	void OnPlayerShot(const FShotPoint& ShotPoint);

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
private:
	UPROPERTY()
	AGun* Gun;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AGun> GunClass;

	UPROPERTY()
	ARock* Rock;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ARock> RockClass;

	UPROPERTY(EditAnywhere)
	float MaxVelocity;

    UPROPERTY(EditAnywhere)
    float VelocityDelta;

    UPROPERTY(EditAnywhere)
    float RockSpawnOffset;

	float Velocity;

	FTimerHandle AbilityTimer;

    bool bIsShowingFriends;
    
	void IncreaseVelocity();
	void MoveForward(float AxisValue);
    void MoveRight(float AxisValue);
	void CharacterJump();
	void StartAbility();
	void FinishAbility();
    void ToggleFriendsWidget();
};
