// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Rock.generated.h"

UCLASS()
class SHOOTERGAME_API ARock : public AActor
{
	GENERATED_BODY()
		
public:	
	// Sets default values for this actor's properties
	ARock();

	void FireInDirection(const FVector& ShootDirection, float Velocity) const;

protected:
	UPROPERTY(EditAnywhere)
	USoundBase* HitSound; 
	
	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	USphereComponent* CollisionComponent;

    UPROPERTY(EditAnywhere)
    float Loudness;

    UPROPERTY(EditAnywhere)
    float SoundRange;
    
	// Projectile movement component.
	UPROPERTY(VisibleAnywhere, Category = Movement)
	UProjectileMovementComponent* ProjectileMovementComponent;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
};

