// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Gun.generated.h"

class UParticleSystem;

USTRUCT()
struct FShotPoint
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FVector ShotLocation;
	UPROPERTY()
	FVector ShotDirection;
};

USTRUCT()
struct FNetPosition
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	FVector Location;

	UPROPERTY()
	FRotator Rotation;

	UPROPERTY()
	float Time;
};

UCLASS()
class SHOOTERGAME_API AGun : public AActor
{
	GENERATED_BODY()
	
public:	
	AGun();

	bool GetShotPoint(FShotPoint& ShotPoint) const;

	float GetMaxRange() const;

	float GetDamage() const;

	float GetDamageVariance() const;

	void SetHitPoint(const FNetPosition& HitPoint);

	void ToggleLastShotFlag();

	void PlayFireEffects() const;

	void PlayImpactEffects(const FNetPosition& HitPoint) const;

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const override;
private:
	UPROPERTY(VisibleAnywhere)
	USceneComponent* Root;

	UPROPERTY(VisibleAnywhere)
	USkeletalMeshComponent* Mesh;

	UPROPERTY(EditAnywhere)
	UParticleSystem* MuzzleFlash;

	UPROPERTY(EditAnywhere)
	USoundBase*  MuzzleSound; 

	UPROPERTY(EditAnywhere)
	UParticleSystem* HitEffect;

	UPROPERTY(EditAnywhere)
	USoundBase* HitSound;
	
	UPROPERTY(EditAnywhere)
	float MaxRange;

	UPROPERTY(EditAnywhere)
	float Damage;

	UPROPERTY(EditAnywhere)
	float DamageVariance;

	UPROPERTY(ReplicatedUsing=OnHit)
	FNetPosition LastHitPoint;

	UPROPERTY(ReplicatedUsing=OnShot)
	bool LastShotFlag;

	UFUNCTION()
	void OnHit() const;

	UFUNCTION()
	void OnShot() const;
};
