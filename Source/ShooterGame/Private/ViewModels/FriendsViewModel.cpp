// Fill out your copyright notice in the Description page of Project Settings.


#include "ViewModels/FriendsViewModel.h"

void UFriendsViewModel::SetDB(UFriendsModel* FriendsModel)
{
    DB = FriendsModel;
    DB->OnFriendConnectionEvent.AddDynamic(this, &UFriendsViewModel::OnFriendConnectionEvent);
}

TArray<UFriendsListItemView*> UFriendsViewModel::GetOnlineFriends()
{
    return GetFriendsStrategy(true);
}

TArray<UFriendsListItemView*> UFriendsViewModel::GetOfflineFriends()
{
    return GetFriendsStrategy(false);
}

TArray<UFriendsListItemView*> UFriendsViewModel::GetFriendsStrategy(bool bShowOnline)
{
   if (!DB->IsConnected())
    {
        TryDBConnection();
    }

    TArray<FFriends*> Friends = DB->GetFriendsList();

    TArray<UFriendsListItemView*> Result;
    for (FFriends* Friend : Friends)
    {
        if (Friend->bConnected == bShowOnline)
        {
            UFriendsListItemView* FriendView = NewObject<UFriendsListItemView>();
            FriendView->Name = Friend->Name;
            FriendView->Platform = Friend->Platform;

            Result.Add(FriendView);
        }
    }
    return Result;
}

void UFriendsViewModel::TryDBConnection()
{
    while (!DB->IsConnected())
    {
        DB->Connect();
    }
}

void UFriendsViewModel::OnFriendConnectionEvent(FName FriendName, bool bIsConnecting)
{
    if (OnPlayerConnectionEvent.IsBound())
    {
        OnPlayerConnectionEvent.Broadcast(FriendName, bIsConnecting);
    }
}

FFriendTooltipView UFriendsViewModel::GetFriend(const FName& Id)
{
    FFriendTooltipView FriendDTO;
    FFriends *Friend = DB->GetFriend(Id);
    if (Friend)
    {
        FriendDTO.Name = Friend->Name;
        FriendDTO.Platform = Friend->Platform;
        FriendDTO.bConnected = Friend->bConnected;
        FriendDTO.LastTimeOnline = Friend->LastConnection;
    }

    return FriendDTO;
}

