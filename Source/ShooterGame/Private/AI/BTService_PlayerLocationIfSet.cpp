// Fill out your copyright notice in the Description page of Project Settings.

#include "BTService_PlayerLocationIfSet.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"

#include "ShooterCharacter.h"

UBTService_PlayerLocationIfSet::UBTService_PlayerLocationIfSet() 
{
    NodeName = TEXT("Player Location if seen");
}

void UBTService_PlayerLocationIfSet::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) 
{
    Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
    
    AAIController* Controller = OwnerComp.GetAIOwner();
    if(Controller == nullptr) 
    {
        return;
    }

    AShooterCharacter* PlayerCharacter = Cast<AShooterCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
    if(PlayerCharacter == nullptr)
    {
        OwnerComp.GetBlackboardComponent()->ClearValue(GetSelectedBlackboardKey()); 
        return;
    }

    if (Controller->LineOfSightTo(PlayerCharacter) && !PlayerCharacter->IsDead())
    {
        OwnerComp.GetBlackboardComponent()->SetValueAsObject(GetSelectedBlackboardKey(), PlayerCharacter);
    } else {
        OwnerComp.GetBlackboardComponent()->ClearValue(GetSelectedBlackboardKey());
    }  
}

