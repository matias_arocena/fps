

#include "BTTask_Shoot.h"

#include "AIController.h"

#include "ShooterCharacter.h"


UBTTask_Shoot::UBTTask_Shoot() 
{
    NodeName = TEXT("Shoot");
}

EBTNodeResult::Type UBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) 
{
    Super::ExecuteTask(OwnerComp, NodeMemory);
    AAIController* Controller = OwnerComp.GetAIOwner();
    if (Controller == nullptr) 
    {
        return EBTNodeResult::Failed;
    }

    AShooterCharacter* Character = Cast<AShooterCharacter>(Controller->GetPawn());
    if (Character == nullptr) 
    {
        return EBTNodeResult::Failed;
    }
    
    Character->ShotGun();
    return EBTNodeResult::Succeeded;
}
