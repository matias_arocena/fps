// Fill out your copyright notice in the Description page of Project Settings.

#include "Models/FriendsModel.h"

#include <time.h>

#include "Async/Async.h"
#include "Async/TaskGraphInterfaces.h"
#include "Engine/GameInstance.h"
#include "TimerManager.h"

bool UFriendsModel::Connect()
{
    FPlatformProcess::Sleep(1.f);
    
    if (rand() % 4 != 1)
    {
        LoadDB();
        bConnected = true;
    }

    bIsGameRunning = true;
    UpdateDB();
    
    return bConnected;
}

bool UFriendsModel::IsConnected() const
{
    return bConnected;
}

TArray<FFriends *> UFriendsModel::GetFriendsList() const
{
    TArray<FFriends *> FriendsList;
    if(bConnected)
    {
        FString ContextString;
       
        FRWScopeLock Lock(FriendsDataMutex, FRWScopeLockType::SLT_ReadOnly);
        FriendsData->GetAllRows<FFriends>(ContextString, FriendsList);
    }
    return FriendsList;
}

void UFriendsModel::LoadDB()
{
    FRWScopeLock Lock(FriendsDataMutex, FRWScopeLockType::SLT_Write);
    if (!FriendsData)
    {
        FriendsData = NewObject<UDataTable>();
        FriendsData->RowStruct = FFriends::StaticStruct();
    }

    for (int i = 0; i < 10; ++i)
    {
        FFriends NewFriend = GenerateUser(i);
        FriendsData->AddRow(FName(NewFriend.Name), NewFriend);
    } 
}

FFriends UFriendsModel::GenerateUser(int Id) const
{
    FFriends NewFriend;
    NewFriend.Name = FString::Printf(TEXT("user%i"), Id);
    NewFriend.bConnected = rand() % 3 == 1;
    NewFriend.LastConnection = FDateTime::MaxValue(); 
        
    int ChoosePlatform = rand() % 4;
    switch (ChoosePlatform) {
    case 1:
        NewFriend.Platform = EPlatform::WIN;
        break;
    case 2:
        NewFriend.Platform = EPlatform::UNIX;
        break;
    case 3:
        NewFriend.Platform = EPlatform::PS;
        break;
    case 4:
        NewFriend.Platform = EPlatform::SWITCH;
        break;
    }
    return NewFriend;
}

void UFriendsModel::UpdateDB()
{
    // Checkear thread
    AsyncTask(ENamedThreads::AnyBackgroundHiPriTask, [this] {
        while (bIsGameRunning) {
            TArray<FName> RowsToToggle;

            FString ContextString;
            FriendsData->ForeachRow<FFriends>(ContextString, [&](const FName Row, const FFriends Friend){
                if (rand() % 8 == 0)
                {
                    RowsToToggle.Add(Row);
                }
            });

            for (FName Row : RowsToToggle)
            {
                AsyncTask(ENamedThreads::GameThread, [&] {
                    ToggleConnection(Row);
                });
            }

            float SleepTime = 10 + (rand() % 30) / 10.f;
            FPlatformProcess::Sleep(SleepTime);
        }
    });
}


void UFriendsModel::ToggleConnection(const FName& Row) const
{

    FRWScopeLock Lock(FriendsDataMutex, FRWScopeLockType::SLT_Write);

    FString ContextString;
    FFriends* Friends = FriendsData->FindRow<FFriends>(Row, ContextString);
 
    FFriends NewFriend;
    NewFriend.Name = Friends->Name;
    NewFriend.Platform = Friends->Platform;
    NewFriend.bConnected = !Friends->bConnected;
    if (NewFriend.bConnected)
    {
        NewFriend.LastConnection = FDateTime::MaxValue();
    } else {
        NewFriend.LastConnection = FDateTime::Now();
    }
    
    FriendsData->RemoveRow(Row);
    FriendsData->AddRow(Row, NewFriend);

    // Bound for reference. Its safe to call a broadcast delegate without checking if its bound but I should check for singlecast delegates
    if (OnFriendConnectionEvent.IsBound() && bIsGameRunning)
    {
        OnFriendConnectionEvent.Broadcast(Row, NewFriend.bConnected);
    }
}

FFriends *UFriendsModel::GetFriend(FName Id) const {
    FString ContextString;
    return FriendsData->FindRow<FFriends>(Id, ContextString);
}

void UFriendsModel::Shutdown()
{
    bIsGameRunning = false;
}

