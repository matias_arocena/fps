// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Widgets/ConnectedFriendsWidget.h"

#include "ShooterGameInstance.h"
#include "UObject/UObjectIterator.h"

void UConnectedFriendsWidget::OnFriendConnectionEvent(FName Id, bool bJustConnected) {
    if (bJustConnected)
    {
        TArray<UObject*> OfflineFriends = OfflineFriendsList->GetListItems();
        for (UObject* ListItem : OfflineFriends)
        {
            UFriendsListItemView* Friend = Cast<UFriendsListItemView>(ListItem);
            if (Friend->Name == Id.ToString())
            {
                OfflineFriendsList->RemoveItem(ListItem);
                OnlineFriendsList->AddItem(ListItem);
                break;
            }
        }
    } else {
        TArray<UObject*> OnlineFriends = OnlineFriendsList->GetListItems();
        for (UObject* ListItem : OnlineFriends)
        {
            UFriendsListItemView* Friend = Cast<UFriendsListItemView>(ListItem);
            if (Friend->Name == Id.ToString())
            {
                OnlineFriendsList->RemoveItem(ListItem);
                OfflineFriendsList->AddItem(ListItem);
                break;
            }
        }
    }
}

void UConnectedFriendsWidget::NativeConstruct() 
{
    Super::NativeConstruct();
    
    UShooterGameInstance* GameInstance = Cast<UShooterGameInstance>(GetGameInstance()); 
    if (!GameInstance)
    {
        return;
    }

    ViewModel = GameInstance->GetFriendsViewModel();
    if (!ViewModel)
    {
        return;
    }
    
    TArray<UFriendsListItemView*> OfflineFriends = ViewModel->GetOfflineFriends();
    OfflineFriendsList->SetListItems(OfflineFriends);
    
    TArray<UFriendsListItemView*> OnlineFriends = ViewModel->GetOnlineFriends();
    OnlineFriendsList->SetListItems(OnlineFriends);

    ToggleOnlineListButton->OnClicked.AddDynamic(this, &UConnectedFriendsWidget::ToggleOnlineList);
    ToggleOfflineListButton->OnClicked.AddDynamic(this, &UConnectedFriendsWidget::ToggleOfflineList);


    if (FriendTooltipClass)
    {
        FriendTooltip = CreateWidget<UFriendTooltip>(this, FriendTooltipClass);
        SetToolTip(FriendTooltip);
    }
    
    OnlineFriendsList->OnItemIsHoveredChanged().AddUObject(this, &UConnectedFriendsWidget::OnHoverFriend);
    OfflineFriendsList->OnItemIsHoveredChanged().AddUObject(this, &UConnectedFriendsWidget::OnHoverFriend);
    
    ViewModel->OnPlayerConnectionEvent.AddDynamic(this, &UConnectedFriendsWidget::OnFriendConnectionEvent);
}

void UConnectedFriendsWidget::ToggleOnlineList()
{
    if (!OnlineFriendsList->IsValidLowLevel())
    {
        return;
    }

    if (OnlineFriendsList->IsVisible())
    {
        OnBeforeToggleList(OnlineFriendsList, true);
        OnlineFriendsList->SetVisibility(ESlateVisibility::Collapsed);
        OnAfterToggleList(OnlineFriendsList, true);
        
    } else {
        OnBeforeToggleList(OfflineFriendsList, false);
        OnlineFriendsList->SetVisibility(ESlateVisibility::Visible);
        OnAfterToggleList(OfflineFriendsList, false);
    }
}

void UConnectedFriendsWidget::ToggleOfflineList()
{
    if (!OfflineFriendsList->IsValidLowLevel())
    {
        return;
    }        
        
    if (OfflineFriendsList->IsVisible())
    {
        OnBeforeToggleList(OfflineFriendsList, true);
        OfflineFriendsList->SetVisibility(ESlateVisibility::Collapsed);
        OnAfterToggleList(OfflineFriendsList, true);
    } else {
        OnBeforeToggleList(OfflineFriendsList, false);
        OfflineFriendsList->SetVisibility(ESlateVisibility::Visible);
        OnAfterToggleList(OfflineFriendsList, false);
    }
 
}
void UConnectedFriendsWidget::ReleaseSlateResources(bool bRealeaseChildrens)
{
    Super::ReleaseSlateResources(bRealeaseChildrens);

    if (ToggleOfflineListButton)
    {
        ToggleOfflineListButton->OnClicked.Clear();
    }

    if (ToggleOnlineListButton)
    {
        ToggleOnlineListButton->OnClicked.Clear();
    }

    if (ViewModel)
    {
        ViewModel->OnPlayerConnectionEvent.RemoveDynamic(this, &UConnectedFriendsWidget::OnFriendConnectionEvent);
    }
}

void UConnectedFriendsWidget::OnHoverFriend(UObject* FriendEntry, bool bIsOnline)
{
    if (!FriendEntry || !FriendTooltip)
    {
        return;
    }
    UFriendsListItemView* ListItem = Cast<UFriendsListItemView>(FriendEntry);

    const FFriendTooltipView LastFriendHover = ViewModel->GetFriend(FName(*ListItem->Name));
    FriendTooltip->SetFriend(LastFriendHover);
}

