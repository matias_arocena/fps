// Fill out your copyright notice in the Description page of Project Settings.


#include "EndGameWidget.h"

void UEndGameWidget::SetScreen(const EScreenType ScreenType, const int TotalSeconds) 
{
    this->Count = TotalSeconds;

    if (ScreenType == EScreenType::WinScreen) 
    {
        TxtEndGameTitle->SetText(WinTitle);
        TxtEndGameTitle->SetColorAndOpacity(WinColor);
        TxtEndGameText->SetColorAndOpacity(WinColor);
    } else {
        TxtEndGameTitle->SetText(LoseTitle);
        TxtEndGameTitle->SetColorAndOpacity(LoseColor);
        TxtEndGameText->SetColorAndOpacity(LoseColor);
    }

    TxtEndGameText->SetText(FText::Format(Text, Count));
}

void UEndGameWidget::DecreaseCount() 
{
    Count--;
    TxtEndGameText->SetText(FText::Format(Text, Count));
}

