// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Widgets/FriendEventToast.h"

void UFriendEventToast::SetText(const FString& Name)
{
    PlayerName->SetText(FText::AsCultureInvariant(Name));
}

void UFriendEventToast::ReleaseSlateResources(bool bRealeaseChildrens)
{
    Super::ReleaseSlateResources(bRealeaseChildrens);
    UE_LOG(LogTemp, Warning, TEXT("Toast slate just released"));
}
