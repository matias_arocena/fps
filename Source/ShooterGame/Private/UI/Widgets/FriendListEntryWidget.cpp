// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Widgets/FriendListEntryWidget.h"

#include "Engine/Texture2D.h"
#include "FriendsViewModel.h"

void UFriendListEntryWidget::NativeOnListItemObjectSet(UObject* ListItemObject)
{
    UFriendsListItemView* ListItem = Cast<UFriendsListItemView>(ListItemObject);
    if (ListItem)
    {
        FriendNameText->SetText(FText::AsCultureInvariant(ListItem->Name));

        switch (ListItem->Platform) {
        case EPlatform::WIN:
            SetImageFromIcon(PCIcon);
            break;
        case EPlatform::UNIX:
            SetImageFromIcon(UnixIcon);
            break;
        case EPlatform::PS:
            SetImageFromIcon(PSIcon);
            break;
        case EPlatform::SWITCH:
            SetImageFromIcon(SwitchIcon);
            break;
        }
    }
}

void UFriendListEntryWidget::SetImageFromIcon(const TSoftObjectPtr<UTexture2D>& Icon)
{
    UTexture2D* Image = Icon.LoadSynchronous();
    
    FSlateBrush ImageBrush{};
    ImageBrush.SetImageSize(FVector2D{(float)Image->GetSizeX(), (float)Image->GetSizeY()});
    ImageBrush.SetResourceObject(Image);
    
    PlatformIcon->SetBrush(ImageBrush);
}
