// Fill out your copyright notice in the Description page of Project Settings.


#include "WaitScreenWidget.h"

void UWaitScreenWidget::SetScreen(const EWaitMode WaitMode)
{
	if (WaitMode == EWaitMode::WaitingForPlayers)
	{
		TxtWaitingTitle->SetText(WaitTitle);
		TxtWaitingTitle->SetColorAndOpacity(WaitColor);
		TxtWaitingText->SetText(WaitText);
		TxtWaitingText->SetColorAndOpacity(WaitColor);
	} else
	{
		TxtWaitingTitle->SetText(LoseTitle);
		TxtWaitingTitle->SetColorAndOpacity(LoseColor);
		TxtWaitingText->SetText(LoseText);
		TxtWaitingText->SetColorAndOpacity(LoseColor);
	}
}
