// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuWidget.h"

#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Net/UnrealNetwork.h"

#include <ShooterGameInstance.h>

UMainMenuWidget::UMainMenuWidget(const FObjectInitializer& ObjectInitializer) : UUserWidget(ObjectInitializer)
{
}

void UMainMenuWidget::NativeConstruct() 
{
    Super::NativeConstruct();

    GEngine->GetFirstLocalPlayerController(GetWorld())->bShowMouseCursor = true;
    
    StartSinglePlayerButton->OnClicked.AddDynamic(this, &UMainMenuWidget::StartSinglePlayerGame);

    JoinMultiPlayerButton->OnClicked.AddDynamic(this, &UMainMenuWidget::JoinMultiPlayerGame);

    StartLocalMultiPlayerButton->OnClicked.AddDynamic(this, &UMainMenuWidget::StartLocalMultiPlayerGame);

    QuitButton->OnClicked.AddDynamic(this, &UMainMenuWidget::QuitGame);
}

void UMainMenuWidget::StartSinglePlayerGame() 
{
    if(SinglePlayerLevel.IsValid()) 
    {
        GEngine->GetFirstLocalPlayerController(GetWorld())->ClientTravel(SinglePlayerLevel.GetLongPackageName(), ETravelType::TRAVEL_Absolute);
    }
}

void UMainMenuWidget::JoinMultiPlayerGame()
{
    UShooterGameInstance* GameInstance = Cast<UShooterGameInstance>(GetGameInstance());
    if (GameInstance == nullptr)
    {
	    return;
    }
    GameInstance->JoinServer();
}

void UMainMenuWidget::StartLocalMultiPlayerGame()
{
	if(LocalMultiPlayerLevel.IsValid()) 
    {
        GEngine->GetFirstLocalPlayerController(GetWorld())->ClientTravel(LocalMultiPlayerLevel.GetLongPackageName(), ETravelType::TRAVEL_Absolute);
    }
}

void UMainMenuWidget::QuitGame() 
{
    APlayerController* Controller = GEngine->GetFirstLocalPlayerController(GetWorld());
    UKismetSystemLibrary::QuitGame(this, Controller, EQuitPreference::Quit, false);
}
