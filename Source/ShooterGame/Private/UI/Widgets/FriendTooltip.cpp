// Fill out your copyright notice in the Description page of Project Settings.


#include "FriendTooltip.h"

void UFriendTooltip::SetFriend(const FFriendTooltipView& Friend)
{
    FriendName->SetText(FText::AsCultureInvariant(Friend.Name));
    if (Friend.bConnected)
    {
        ConnectionStatus->SetText(FText::AsCultureInvariant(TEXT("Online")));
    } else if (Friend.LastTimeOnline == FDateTime::MaxValue()) {
        ConnectionStatus->SetText(FText::AsCultureInvariant(TEXT("Long time ago")));
    } else {
        FTimespan FromTimespan(Friend.LastTimeOnline.GetTicks());
        FTimespan Now(FDateTime::Now().GetTicks());

        FTimespan Result = Now - FromTimespan;
        if (Result.GetMinutes() > 0)
        {
            ConnectionStatus->SetText(FText::AsCultureInvariant(FString::Printf(TEXT("Last connection was %i minutes and %i seconds ago."), Result.GetMinutes(), Result.GetSeconds())));
        } else {
            ConnectionStatus->SetText(FText::AsCultureInvariant(FString::Printf(TEXT("Last connection was %i seconds ago."), Result.GetSeconds())));
        }
    }
}
