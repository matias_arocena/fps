// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameWidget.h"

void UInGameWidget::SetHealthPercentage(float HealthPercentage) 
{
    HealthBar->SetPercent(HealthPercentage);   
}
