
// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Widgets/FriendEventWidget.h"
#include "TimerManager.h"

void UFriendEventWidget::OnFriendConnectionEvent(FName Id, bool bJustConnected)
{
    WidgetPool.SetWorld(GetWorld());

    UUserWidget* Widget = WidgetPool.GetOrCreateInstance(FriendEventToastWidgetClass);
    if (!Widget)
    {
        return;
    }

    UFriendEventToast* Toast = Cast<UFriendEventToast>(Widget);
    if (!Widget)
    {
        return;
    }

    if (bJustConnected) 
    {
        Toast->SetText(FString::Printf(TEXT("Player %s is online"), *Id.ToString()));
    } else {
        Toast->SetText(FString::Printf(TEXT("Player %s is offline"), *Id.ToString()));
    }

    Toasts.Add(Toast);

    OnBeforeNewToast();
    VerticalBox->AddChildToVerticalBox(Toast);
    OnAfterNewToast();

    FTimerHandle ClearToastHandle;

    GetWorld()->GetTimerManager().SetTimer(ClearToastHandle, this, &UFriendEventWidget::ClearToast, ToastDuration);
}

void UFriendEventWidget::ClearToast()
{
    OnBeforeClearToast();
    UFriendEventToast* Toast = Toasts[0];
    VerticalBox->RemoveChild(Toast);
    WidgetPool.Release(Toast);
    Toasts.Remove(Toast);
    OnAfterClearToast();
}

void UFriendEventWidget::NativeDestruct()
{
    WidgetPool.ReleaseAll(true);
}
