// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuHUD.h"

#include "MainMenuWidget.h"

void AMainMenuHUD::BeginPlay() 
{
    Super::BeginPlay();

    if (MainMenuWidgetClass) 
    {
        MainMenuWidget = CreateWidget<UMainMenuWidget>(GetWorld(), MainMenuWidgetClass);
        if (MainMenuWidget) 
        {
            MainMenuWidget->AddToPlayerScreen();
        }
    }
}
