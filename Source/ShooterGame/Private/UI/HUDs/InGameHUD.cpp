// Fill out your copyright notice in the Description page of Project Settings.
#include "InGameHUD.h"

#include "EndGameWidget.h"
#include "InGameWidget.h"
#include "ShooterGameInstance.h"


AInGameHUD::AInGameHUD() : AHUD()
{
    State = EGameState::Playing;
    
    StateMutex = new FCriticalSection();
}

AInGameHUD::AInGameHUD(EGameState State)
{
    this->State = State;
    StateMutex = new FCriticalSection();
}

void AInGameHUD::BeginPlay() 
{
    Super::BeginPlay();

    if (!FriendsEventWidget)
    {
        FriendsEventWidget = CreateWidget<UFriendEventWidget>(GetOwningPlayerController(), FriendsEventWidgetClass);
        UShooterGameInstance* GameInstance = Cast<UShooterGameInstance>(GetGameInstance());
        GameInstance->GetFriendsViewModel()->OnPlayerConnectionEvent.AddDynamic(FriendsEventWidget, &UFriendEventWidget::OnFriendConnectionEvent);
        FriendsEventWidget->AddToPlayerScreen();
    }
    
    StartGame();
}

void AInGameHUD::StartGame() 
{
    FScopeLock ScopeLock(StateMutex);

    if (State == EGameState::EndGame && EndGameWidget) 
    {
        EndGameWidget->RemoveFromParent();
    }           
    State = EGameState::Playing;	    

    if (InGameWidgetClass && InGameWidget == nullptr) 
    {
        InGameWidget = CreateWidget<UInGameWidget>(GetOwningPlayerController(), InGameWidgetClass);       
    } 
    if (InGameWidget && !InGameWidget->IsInViewport()) 
    {
        InGameWidget->SetHealthPercentage(1.f);
        InGameWidget->AddToPlayerScreen();
    }
}

void AInGameHUD::EndGame(const bool bIsWinner, const int Count) 
{
    FScopeLock ScopeLock(StateMutex);

	if (State == EGameState::Playing) {
        InGameWidget->RemoveFromParent();
    }
	if (State != EGameState::EndGame)
    {
    	State = EGameState::EndGame;
		
	    if (EndGameWidgetClass && EndGameWidget == nullptr) {
	        EndGameWidget = CreateWidget<UEndGameWidget>(GetOwningPlayerController(), EndGameWidgetClass);
	    }   
        if (!EndGameWidget->IsInViewport()) {
	        EndGameWidget->AddToPlayerScreen();
	        if (bIsWinner) {
	            EndGameWidget->SetScreen(EScreenType::WinScreen, Count);
	        } else {
				EndGameWidget->SetScreen(EScreenType::LoseScreen, Count);
			}
	    }
    }
}

void AInGameHUD::SetHealthPercentage(float HealthPercentage) const
{
    if (InGameWidget) {
        InGameWidget->SetHealthPercentage(HealthPercentage);
    }
}

void AInGameHUD::DecreaseCount() const
{
    if (EndGameWidget) {
        EndGameWidget->DecreaseCount();
    }
}

void AInGameHUD::ToggleFriendsWidget()
{
    if (FriendsWidgetClass && FriendsWidget == nullptr)
    {
        FriendsWidget = CreateWidget<UConnectedFriendsWidget>(GetOwningPlayerController(), FriendsWidgetClass);
    }
    if (FriendsWidget && !FriendsWidget->IsInViewport())
    {
        FriendsWidget->AddToPlayerScreen();
        APlayerController* PC = GEngine->GetFirstLocalPlayerController(GetWorld());
        if (!PC)
        {
            return;
        }
        PC->bShowMouseCursor = true;
        PC->SetIgnoreLookInput(true);
        PC->SetIgnoreMoveInput(true);
    } else if (FriendsWidget) {
        FriendsWidget->RemoveFromParent();
        GEngine->GetFirstLocalPlayerController(GetWorld())->bShowMouseCursor = false;
        APlayerController* PC = GetOwningPlayerController();
        if (!PC)
        {
            return;
        }
        PC->bShowMouseCursor = false;
        PC->SetIgnoreLookInput(false);
        PC->SetIgnoreMoveInput(false);
    }
}
