// Fill out your copyright notice in the Description page of Project Settings.


#include "NetworkInGameHUD.h"

#include "InGameWidget.h"
#include "WaitScreenWidget.h"

ANetworkInGameHUD::ANetworkInGameHUD() : AInGameHUD(EGameState::Waiting)
{
}

void ANetworkInGameHUD::StartGame()
{
	FScopeLock ScopeLock(StateMutex);

    if (State == EGameState::Waiting && WaitingWidgetClass && WaitScreenWidget == nullptr)
	{
		WaitScreenWidget = CreateWidget<UWaitScreenWidget>(GetWorld(), WaitingWidgetClass);
	}		

	if (WaitScreenWidget && !WaitScreenWidget->IsInViewport())
	{
		WaitScreenWidget->SetScreen(EWaitMode::WaitingForPlayers);
		WaitScreenWidget->AddToViewport();
	}
}

void ANetworkInGameHUD::EndGame(const bool bIsWinner, const int Count)
{
	{
		FScopeLock ScopeLock(StateMutex);
		if (WaitScreenWidget && WaitScreenWidget->IsInViewport())
		{
			WaitScreenWidget->RemoveFromViewport();
		}
		if (InGameWidget && InGameWidget->IsInViewport())
		{
			InGameWidget->RemoveFromViewport();
		}
	}

	Super::EndGame(bIsWinner, Count);
}

void ANetworkInGameHUD::WaitUntilMatchEnds()
{
	FScopeLock ScopeLock(StateMutex);
	if (State == EGameState::Playing)
	{
		State = EGameState::Waiting;

		if (WaitingWidgetClass && WaitScreenWidget == nullptr)
		{
			WaitScreenWidget = CreateWidget<UWaitScreenWidget>(GetWorld(), WaitingWidgetClass);
		}		

		if (WaitScreenWidget && !WaitScreenWidget->IsInViewport())
		{
			WaitScreenWidget->SetScreen(EWaitMode::WaitingForPlayers);
			WaitScreenWidget->AddToViewport();
		}
	}
}

void ANetworkInGameHUD::StartMatch()
{
	{
		FScopeLock ScopeLock(StateMutex);
		if (State == EGameState::Waiting && WaitScreenWidget)
		{
			WaitScreenWidget->RemoveFromViewport();
		}
	}

	Super::StartGame();
}
