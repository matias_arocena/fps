// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"

#include "Animation/AnimInstance.h"
#include "Animation/AnimMontage.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Net/UnrealNetwork.h"

#include "Gun.h"
#include "InGameHUD.h"
#include "ShooterGameGameModeBase.h"
#include "TimerManager.h"

AShooterCharacter::AShooterCharacter() : MaxHealth { 100.f }, Velocity { 0.f }, bIsShowingFriends { false }
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
}

bool AShooterCharacter::IsDead() const
{
	return Health <= 0;	
}

float AShooterCharacter::GetHealthPercentage() const
{
	return Health/MaxHealth;	
}

void AShooterCharacter::SetHealth(float HealthValue)
{
	this->Health = FMath::Clamp(HealthValue, 0.f, MaxHealth);
	OnHealthUpdate();
}

void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	SetHealth(MaxHealth);	

	Gun = GetWorld()->SpawnActor<AGun>(GunClass);

	GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);
	Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponSocket"));
	Gun->SetOwner(this);
}

void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	APlayerController* PC = Cast<APlayerController>(GetController());

	int ControllerId = 0;
	if (PC)
	{
		ULocalPlayer* Player = PC->GetLocalPlayer();
		if (Player)
		{
			ControllerId = Player->GetControllerId();
		}
	}

    PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterCharacter::MoveForward);
    PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterCharacter::MoveRight);
    PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
    PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);
    PlayerInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &AShooterCharacter::CharacterJump);
    PlayerInputComponent->BindAction(TEXT("Shot"), EInputEvent::IE_Pressed, this, &AShooterCharacter::ShotGun);
    PlayerInputComponent->BindAction(TEXT("Ability"), EInputEvent::IE_Pressed, this, &AShooterCharacter::StartAbility);
    PlayerInputComponent->BindAction(TEXT("Ability"), EInputEvent::IE_Released, this, &AShooterCharacter::FinishAbility);
    PlayerInputComponent->BindAction(TEXT("ShowFriends"), EInputEvent::IE_Pressed, this, &AShooterCharacter::ToggleFriendsWidget);
}

void AShooterCharacter::MoveForward(float AxisValue) 
{
	AddMovementInput(GetActorForwardVector() * AxisValue);	
}

void AShooterCharacter::MoveRight(float AxisValue) 
{
	AddMovementInput(GetActorRightVector() * AxisValue);
}

void AShooterCharacter::CharacterJump()
{
	if(CanJump()) 
	{
		Jump();
	}
}

void AShooterCharacter::StartAbility()
{
	if (AbilityMontage)
	{
		PlayAnimMontage(AbilityMontage);
	}

	GetWorldTimerManager().SetTimer(AbilityTimer, this, &AShooterCharacter::IncreaseVelocity, .5f, true, 0.f);
}


void AShooterCharacter::FinishAbility()
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if(AnimInstance && AbilityMontage)
	{
		AnimInstance->Montage_Resume(AbilityMontage);
	}

	GetWorldTimerManager().ClearTimer(AbilityTimer);
	
	FVector Location;
	FRotator Rotation;
	GetController()->GetPlayerViewPoint(Location, Rotation);

	FActorSpawnParameters SpawnParams;
    SpawnParams.Owner = this;
    SpawnParams.Instigator = GetInstigator();

	FVector SpawnLocation = GetMesh()->GetSocketLocation(TEXT("WeaponSocket")) + Rotation.Vector() * RockSpawnOffset;
	
	Rock = GetWorld()->SpawnActor<ARock>(RockClass, SpawnLocation, Rotation, SpawnParams);
	if (Rock)
	{
		Rock->FireInDirection(Rotation.Vector(), Velocity);
	}
	Velocity = 0;
}

void AShooterCharacter::OnAbilityNotify()
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && AbilityMontage) {
		AnimInstance->Montage_Pause(AbilityMontage);
	}
}

void AShooterCharacter::IncreaseVelocity()
{
	if (Velocity == 0)
	{
		Velocity = VelocityDelta;
	} else if (Velocity < MaxVelocity){
		Velocity += VelocityDelta;
	}
}

void AShooterCharacter::ShotGun() 
{
    if (bIsShowingFriends)
    {
        return;
    }

	if (GetNetMode() != NM_ListenServer)
	{
		Gun->PlayFireEffects();
	}

	FShotPoint ShotPoint;
	if (Gun->GetShotPoint(ShotPoint))
	{
		OnPlayerShot(ShotPoint);

		if (GetNetMode() != NM_ListenServer)
		{
			Gun->PlayFireEffects();
		}
	}
}

void AShooterCharacter::GetLifetimeReplicatedProps(TArray <FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AShooterCharacter, Health);
}

void AShooterCharacter::OnHealthUpdate() {
	APlayerController* PlayerController = Cast<APlayerController>(GetController());
	if(PlayerController) {
		AInGameHUD* InGameHUD = Cast<AInGameHUD>(PlayerController->GetHUD());
		if (InGameHUD) {
			InGameHUD->SetHealthPercentage(GetHealthPercentage());
		}
	}

	if(IsDead()) {
		AShooterGameGameModeBase* GameMode = GetWorld()->GetAuthGameMode<AShooterGameGameModeBase>(); 
		if(GameMode != nullptr) {
			GameMode->PawnKilled(this);
		}

		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

float AShooterCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	DamageToApply = FGenericPlatformMath::Min(DamageToApply, Health);
	SetHealth(Health - DamageToApply);

	return DamageToApply;
}

void AShooterCharacter::OnPlayerShot_Implementation(const FShotPoint& ShotPoint) {
	Gun->ToggleLastShotFlag();
	Gun->PlayFireEffects();

	const FVector End = ShotPoint.ShotLocation + ShotPoint.ShotDirection * Gun->GetMaxRange();
	
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());

	FHitResult Hit;
	GetWorld()->LineTraceSingleByChannel(Hit, ShotPoint.ShotLocation, End, ECollisionChannel::ECC_GameTraceChannel1, Params);
	if (Hit.Actor == nullptr)
	{
		return;
	}

	AShooterCharacter* Character = Cast<AShooterCharacter>(Hit.GetActor());
	if (Character != nullptr)
	{
		float RealDamage = Gun->GetDamage() + FMath::RandRange(-Gun->GetDamageVariance(), Gun->GetDamageVariance());
		FPointDamageEvent DamageEvent(RealDamage, Hit, ShotPoint.ShotDirection, nullptr);
		Character->TakeDamage(RealDamage, DamageEvent, GetController(), Gun);
	}

	FNetPosition HitPoint;
	HitPoint.Location = Hit.Location;
	HitPoint.Rotation = Hit.ImpactNormal.ToOrientationRotator();
	HitPoint.Time = GetWorld()->GetTimeSeconds();

	Gun->SetHitPoint(HitPoint);
	Gun->PlayImpactEffects(HitPoint);
}

void AShooterCharacter::ToggleFriendsWidget()
{
    APlayerController* PC = Cast<APlayerController>(GetController());
    if (!PC)
    {
        return;
    }

    AInGameHUD* HUD = Cast<AInGameHUD>(PC->GetHUD());
    if (!HUD)
    {
        return;
    }

    HUD->ToggleFriendsWidget();

    bIsShowingFriends = !bIsShowingFriends;
}

