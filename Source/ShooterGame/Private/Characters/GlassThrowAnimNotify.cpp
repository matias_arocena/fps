// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/GlassThrowAnimNotify.h"

#include "Animation/AnimSequenceBase.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/Engine.h"
#include "GameFramework/PlayerController.h"

#include "ShooterCharacter.h"

void UGlassThrowAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	AShooterCharacter* Character = Cast<AShooterCharacter>(MeshComp->GetOwner());
	if (Character)
	{
		Character->OnAbilityNotify();
	}
}

 
