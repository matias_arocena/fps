// Fill out your copyright notice in the Description page of Project Settings.
#include "Gun.h"

#include "Components/SkeletalMeshComponent.h"
#include "Engine/World.h"
#include "GameFramework/Controller.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"


AGun::AGun() : MaxRange {1000.f}, Damage {10.f}
{
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);

	bReplicates = true;
}

void AGun::PlayFireEffects() const
{
	if (HitEffect)
	{
		UGameplayStatics::SpawnEmitterAttached(HitEffect, Mesh, TEXT("weapon_r"));
	}

	if (HitSound)
	{
		UGameplayStatics::SpawnSoundAttached(HitSound, Mesh, TEXT("weapon_r"));
	}
}

void AGun::PlayImpactEffects(const FNetPosition& HitPoint) const
{
	if (MuzzleFlash) 
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), MuzzleFlash, HitPoint.Location, HitPoint.Rotation);
	}

	if (MuzzleSound) 
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), MuzzleSound, HitPoint.Location, HitPoint.Rotation);
	}
}

void AGun::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME_CONDITION(AGun, LastHitPoint, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(AGun, LastShotFlag, COND_SkipOwner);
}

void AGun::OnHit() const
{
	PlayImpactEffects(LastHitPoint);
}

void AGun::OnShot() const
{
	PlayFireEffects();
}

bool AGun::GetShotPoint(FShotPoint& ShotPoint) const
{
	APawn* Pawn = Cast<APawn>(GetOwner());
	if (Pawn == nullptr)
	{
		return false; 
	}

	AController* Controller = Pawn->GetController(); 
	if (Controller == nullptr)
	{
		return false;
	}

	FRotator OutRotator;
	Controller->GetPlayerViewPoint(ShotPoint.ShotLocation, OutRotator);
	ShotPoint.ShotDirection = OutRotator.Vector();
	return true;
}

float AGun::GetMaxRange() const
{
	return MaxRange;
}

float AGun::GetDamage() const
{
	return Damage;
}

float AGun::GetDamageVariance() const
{
	return DamageVariance;
}

void AGun::SetHitPoint(const FNetPosition& HitPoint)
{
	LastHitPoint = HitPoint;
}

void AGun::ToggleLastShotFlag()
{
	LastShotFlag = !LastShotFlag;
}
