// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Rock.h"

#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AISense_Hearing.h"

// Sets default values
ARock::ARock()
{
	PrimaryActorTick.bCanEverTick = true;
	if(!RootComponent)
	{
		RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("ProjectileSceneComponent"));
	}
	if(!CollisionComponent)
	{
		CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
		CollisionComponent->InitSphereRadius(15.0f);

		CollisionComponent->BodyInstance.SetCollisionProfileName(TEXT("BlockAll"));

		CollisionComponent->SetSimulatePhysics(true);
		CollisionComponent->SetNotifyRigidBodyCollision(true);

		CollisionComponent->OnComponentHit.AddDynamic(this, &ARock::OnHit);

		RootComponent = CollisionComponent;
	}
	if(!ProjectileMovementComponent)
	{
		ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
		ProjectileMovementComponent->SetUpdatedComponent(CollisionComponent);
	}

	InitialLifeSpan = 3.0f;
}

void ARock::FireInDirection(const FVector& ShootDirection, float Velocity) const
{
    ProjectileMovementComponent->Velocity = ShootDirection * Velocity;
}

void ARock::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{

    if (OtherActor != GetOwner()) {
        if (HitSound) 
        {
            UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, Hit.Location, Hit.ImpactNormal.Rotation());
            UAISense_Hearing::ReportNoiseEvent(GetWorld(), GetActorLocation(), Loudness, GetWorld()->GetFirstPlayerController(), SoundRange, FName(TEXT("ROCK_NOISE")));
        }
        Destroy();
    }
}
