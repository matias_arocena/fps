// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterAIController.h"

#include "Perception/AIPerceptionComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"

#include "ShooterCharacter.h"

void AShooterAIController::BeginPlay() 
{
    Super::BeginPlay();

    GetPerceptionComponent()->OnTargetPerceptionUpdated.AddDynamic(this, &AShooterAIController::OnStimulus);
    
    if (AIBehavior != nullptr) 
    {
        RunBehaviorTree(AIBehavior);   

        APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
        if  (PlayerPawn != nullptr) 
        {
            GetBlackboardComponent()->SetValueAsVector(TEXT("PlayerLocation"), PlayerPawn->GetActorLocation());
            GetBlackboardComponent()->SetValueAsVector(TEXT("StartLocation"), GetPawn()->GetActorLocation());
        }
    }
}

bool AShooterAIController::IsDead() const
{
    AShooterCharacter* ControlledCharacter = Cast<AShooterCharacter>(GetPawn()); 
    if (ControlledCharacter != nullptr) 
    {
        return ControlledCharacter->IsDead();
    }
    return true; 
}


void AShooterAIController::OnStimulus(AActor* Actor, FAIStimulus Stimulus)
{
    GetBlackboardComponent()->SetValueAsVector(LastSeenBlackboardProperty, Stimulus.StimulusLocation);
}
