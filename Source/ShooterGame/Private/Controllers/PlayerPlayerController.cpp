// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPlayerController.h"

#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"

#include "InGameHUD.h"
#include "ShooterCharacter.h"
#include "NetworkInGameHUD.h"
#include "ShooterGameInstance.h"

APlayerPlayerController::APlayerPlayerController() : RestartDelay{5}
{    
}

void APlayerPlayerController::Kill_Implementation()
{
	ANetworkInGameHUD* InGameHUD = Cast<ANetworkInGameHUD>(GetHUD());
	if (!InGameHUD)
	{
        return;
	}
    InGameHUD->WaitUntilMatchEnds();
}

void APlayerPlayerController::DecreaseRestartCount() 
{
	AInGameHUD* InGameHUD = Cast<AInGameHUD>(GetHUD());
	if (InGameHUD) 
    {
        InGameHUD->DecreaseCount();
    }

    if (--RestartDelay <= 0) 
    {
        GetWorldTimerManager().ClearTimer(RestartTimer);
		
		UShooterGameInstance* GameInstance = Cast<UShooterGameInstance>(GetGameInstance());
        if (GameInstance)
        {
            GameInstance->DestroySession();
        }

		if (this == UGameplayStatics::GetPlayerControllerFromID(GetWorld(), 0))
		{
			ClientTravel(MainMenu.GetLongPackageName(), TRAVEL_Absolute);
		} else
		{
			PawnPendingDestroy(GetPawn());
			GameInstance->RemoveLocalPlayer(GetLocalPlayer());
		}
    }
}

void APlayerPlayerController::GameHasEnded_Implementation(class AActor* EndGameFocus, bool bIsWinner) 
{
    Super::GameHasEnded(EndGameFocus, bIsWinner);
    AInGameHUD* InGameHUD = Cast<AInGameHUD>(GetHUD());
    if (InGameHUD) 
    {
        InGameHUD->EndGame(bIsWinner, RestartDelay);
    }		

	GetWorldTimerManager().SetTimer(RestartTimer, this, &APlayerPlayerController::DecreaseRestartCount, 1.f, true, 0.f);
}

void APlayerPlayerController::MatchStarted_Implementation()
{
	ANetworkInGameHUD* InGameHUD = Cast<ANetworkInGameHUD>(GetHUD());
	if (InGameHUD != nullptr)
	{
		InGameHUD->StartMatch();
	}
}

bool APlayerPlayerController::IsDead() const
{
    AShooterCharacter* PlayerCharacter = Cast<AShooterCharacter>(GetPawn());
    if (PlayerCharacter != nullptr)
    {
	    return PlayerCharacter->IsDead();
    }
    return true;
}

void APlayerPlayerController::SpawnDefaultHUD()
{
	Super::SpawnDefaultHUD();
	AInGameHUD* InGameHUD = Cast<AInGameHUD>(GetHUD());
	if (InGameHUD != nullptr)
	{
		InGameHUD->StartGame();
	}
}

