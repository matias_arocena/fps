// Fill out your copyright notice in the Description page of Project Settings.
#include "KillEmAllGamemode.h"

#include "EngineUtils.h"
#include "GameFramework/Controller.h"

#include "ShooterAIController.h"

void AKillEmAllGamemode::PawnKilled(APawn* PawnKilled) 
{
	Super::PawnKilled(PawnKilled);

    APlayerController* PlayerController = Cast<APlayerController>(PawnKilled->GetController());
	if (PlayerController != nullptr) 
    {
        EndGame(false);
    }

    bool bGameOver = true;
    TActorIterator<AShooterAIController> AIControllerItr(GetWorld());
    while (bGameOver && AIControllerItr) 
    {
        bGameOver = AIControllerItr->IsDead();
        ++AIControllerItr;
    }        

    if(bGameOver) 
    {
        EndGame(true);
    }
}

void AKillEmAllGamemode::EndGame(const bool bIsPlayerWinner) const
{
    for (AController* Controller: TActorRange<AController>(GetWorld()))
    {
        Controller->GameHasEnded(Controller->GetPawn(), Controller->IsPlayerController() == bIsPlayerWinner);   
    }
}
