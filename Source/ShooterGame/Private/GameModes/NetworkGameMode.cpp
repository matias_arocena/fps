// Fill out your copyright notice in the Description page of Project Settings.


#include "NetworkGameMode.h"

#include "EngineUtils.h"

#include "PlayerPlayerController.h"
#include "ShooterGameInstance.h"

ANetworkGameMode::ANetworkGameMode()
{
    CountPlayerMutex = new FCriticalSection();
}

void ANetworkGameMode::PawnKilled(APawn* PawnKilled)
{
	Super::PawnKilled(PawnKilled);

    APlayerPlayerController* CharacterController = Cast<APlayerPlayerController>(PawnKilled->GetController());
    if (CharacterController != nullptr)
    {
        CharacterController->Kill();
	    --RemainingPlayers;
    }

    APlayerPlayerController* WinnerPlayerController = nullptr;

	TActorIterator<APlayerPlayerController> PlayerControllerItr(GetWorld());
    while (PlayerControllerItr)
    {
        if (!PlayerControllerItr->IsDead())
        {
            WinnerPlayerController = *PlayerControllerItr;
        }
        ++PlayerControllerItr;
    }
    
    if (RemainingPlayers == 1 && WinnerPlayerController != nullptr)
    {
	    WinnerPlayerController->GameHasEnded(WinnerPlayerController->GetPawn(), true);

    	PlayerControllerItr = TActorIterator<APlayerPlayerController>(GetWorld());
    	while (PlayerControllerItr)
		{
		    if (PlayerControllerItr->IsDead())
		    {
                PlayerControllerItr->GameHasEnded(PlayerControllerItr->GetPawn(), false);
		    }
		    ++PlayerControllerItr;
		}

    }
}

void ANetworkGameMode::PostLogin(APlayerController* NewPlayer)
{
    Super::PostLogin(NewPlayer);

    FScopeLock ScopeLock(CountPlayerMutex);
	RemainingPlayers = CountPlayers();
}

void ANetworkGameMode::Logout(AController* Exiting)
{
    Super::Logout(Exiting);

    FScopeLock ScopeLock(CountPlayerMutex);
	--RemainingPlayers;
}

bool ANetworkGameMode::ReadyToStartMatch_Implementation()
{
	return RemainingPlayers > 1;
}

void ANetworkGameMode::StartMatch()
{
	Super::StartMatch();
    TActorIterator<APlayerPlayerController> PlayerControllerItr(GetWorld());
    while (PlayerControllerItr)
    {
		PlayerControllerItr->MatchStarted();
        ++PlayerControllerItr;
    }
}

uint8 ANetworkGameMode::CountPlayers() const
{
    uint8 ConnectedPlayers = 0;
	TActorIterator<APlayerPlayerController> PlayerControllerItr(GetWorld());
	while (PlayerControllerItr)
	{
        ++ConnectedPlayers;
		++PlayerControllerItr;
	}

    return ConnectedPlayers;
}
