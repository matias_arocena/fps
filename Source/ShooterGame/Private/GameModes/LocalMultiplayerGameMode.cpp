// Fill out your copyright notice in the Description page of Project Settings.


#include "GameModes/LocalMultiplayerGameMode.h"

#include "EngineUtils.h"
#include "Engine/LocalPlayer.h"
#include "Engine/GameViewportClient.h"
#include "ShooterCharacter.h"
#include "Kismet/GameplayStatics.h"

void ALocalMultiplayerGameMode::BeginPlay()
{
	Super::BeginPlay();
	
	SecondPlayer = UGameplayStatics::CreatePlayer(GetWorld());
}

void ALocalMultiplayerGameMode::PawnKilled(APawn* PawnKilled) 
{
	Super::PawnKilled(PawnKilled);

    APlayerController* PlayerController = Cast<APlayerController>(PawnKilled->GetController());
	if (PlayerController == nullptr) 
    {
        return;
    }
    const APlayerController* Winner = nullptr;
    TActorIterator<APlayerController> ControllerItr(GetWorld());
    while (ControllerItr)
    {
        const AShooterCharacter* Character = Cast<AShooterCharacter>(ControllerItr->GetPawn());
        if (!Character->IsDead())
        {
            Winner = *ControllerItr;
        }
        ++ControllerItr;
    }

    EndGame(Winner);
}

void ALocalMultiplayerGameMode::EndGame(const APlayerController* WinnerPC) const
{
    for (APlayerController* Controller: TActorRange<APlayerController>(GetWorld()))
    {
        Controller->GameHasEnded(Controller->GetPawn(), Controller == WinnerPC);   
    }
}
