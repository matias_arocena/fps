// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterGameInstance.h"

#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"
#include "TimerManager.h"

UShooterGameInstance::UShooterGameInstance() : UGameInstance()
{
	SessionName = FName("ShooterMultiplayer");

    FriendsModelInstanceMutex = new FCriticalSection();
    FriendsModelInstance = nullptr;

    FriendsViewModelInstanceMutex = new FCriticalSection();
    FriendsViewModelInstance = nullptr;
}

void UShooterGameInstance::Init()
{
	if (IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
	{
	  
		OnlineSession = OnlineSubsystem->GetSessionInterface();
		if (OnlineSession.IsValid())
		{
			OnlineSession->OnCreateSessionCompleteDelegates.AddUObject(this, &UShooterGameInstance::OnCreateSessionComplete);
			OnlineSession->OnFindSessionsCompleteDelegates.AddUObject(this, &UShooterGameInstance::OnFindSessionsComplete);
			OnlineSession->OnJoinSessionCompleteDelegates.AddUObject(this, &UShooterGameInstance::OnJoinSessionComplete);
		}
	}
}

void UShooterGameInstance::CreateServer() const
{
	FOnlineSessionSettings SessionSettings;
	SessionSettings.bAllowJoinInProgress = true;
	SessionSettings.bIsDedicated = false;
	SessionSettings.bIsLANMatch = true;
	SessionSettings.bShouldAdvertise = true;
	SessionSettings.bUsesPresence = true;
	SessionSettings.NumPublicConnections = 5;

	OnlineSession->CreateSession(0, SessionName, SessionSettings);
}

void UShooterGameInstance::ConnectToSession()
{
	UE_LOG(LogTemp, Warning, TEXT("Searching for sessions"));

	SessionSearch = MakeShareable(new FOnlineSessionSearch());
	SessionSearch->bIsLanQuery = true;
	SessionSearch->MaxSearchResults = 0;
	SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);

	OnlineSession->FindSessions(0, SessionSearch.ToSharedRef());
}

void UShooterGameInstance::SelfDestruct() const
{
	OnlineSession->DestroySession(SessionName);
}

void UShooterGameInstance::OnCreateSessionComplete(FName ServerName, bool bSucceeded)
{
	UE_LOG(LogTemp, Warning, TEXT("Creation of session %s. Status: %d"), *ServerName.ToString(), bSucceeded);
	if (bSucceeded)
	{
		UE_LOG(LogTemp, Warning, TEXT("Teleporting to %s for session %s"), *MultiPlayerLevel.GetLongPackageName(), *ServerName.ToString());
		GetWorld()->ServerTravel(MultiPlayerLevel.GetLongPackageName() + "?listen");
	}
}

void UShooterGameInstance::OnFindSessionsComplete(bool bSucceeded)
{
	if (bSucceeded && SessionSearch->SearchResults.Num() > 0) 
	{
		UE_LOG(LogTemp, Warning, TEXT("A session was found: %s. Joining."), *SessionSearch->SearchResults[0].Session.GetSessionIdStr());
		OnlineSession->JoinSession(0, SessionName, SessionSearch->SearchResults[0]);				
	} else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to find a session. Creating new server"));
		CreateServer();
	} 
}

void UShooterGameInstance::OnJoinSessionComplete(FName ServerName, EOnJoinSessionCompleteResult::Type Result)
{
	if (Result == EOnJoinSessionCompleteResult::Success)
	{
		if (APlayerController* PC = GetFirstLocalPlayerController(GetWorld()))
		{
			FString JoinAddress;
			OnlineSession->GetResolvedConnectString(ServerName, JoinAddress);
			UE_LOG(LogTemp, Warning, TEXT("Joining address: %s"), *JoinAddress);
			PC->ClientTravel(JoinAddress, TRAVEL_Absolute);
		}
	} 
}

void UShooterGameInstance::JoinServer()
{
	ConnectToSession();
}

void UShooterGameInstance::DestroySession()
{
	FTimerHandle AutoDestroyTimer;
	GetTimerManager().SetTimer(AutoDestroyTimer, this, &UShooterGameInstance::SelfDestruct, 10);
}


UFriendsModel* UShooterGameInstance::GetFriendsModel()
{
    FScopeLock ScopeLock(FriendsModelInstanceMutex);
   
    if (FriendsModelInstance == nullptr)
    {
        FriendsModelInstance = NewObject<UFriendsModel>();
    }
    return FriendsModelInstance;
}

UFriendsViewModel* UShooterGameInstance::GetFriendsViewModel()
{
    FScopeLock ScopeLock(FriendsViewModelInstanceMutex);
    
    if (FriendsViewModelInstance == nullptr)
    {
        FriendsViewModelInstance = NewObject<UFriendsViewModel>();
        FriendsViewModelInstance->SetDB(GetFriendsModel());
    }

    return FriendsViewModelInstance;
}


void UShooterGameInstance::Shutdown()
{
    GetFriendsModel()->Shutdown();
}
